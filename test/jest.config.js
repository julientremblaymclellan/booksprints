global.CONFIG = { 'pubsweet-server': '' }
global.PUBSWEET_COMPONENTS = []

global.mock = {
  data: require('./dataMock'), // eslint-disable-line
  redux: require('./reduxMock'), // eslint-disable-line
}
